package ru.buz.financeinspector.Controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.buz.financeinspector.dao.Models.AccountModel;
import ru.buz.financeinspector.dao.repository.AccountRepository;
import ru.buz.financeinspector.service.AccountDto;
import ru.buz.financeinspector.service.CreateTransactionService;
import ru.buz.financeinspector.service.TransactionDto;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/transaction")
public class TransactionController {
    private final CreateTransactionService createTransactionService;
    private final AccountRepository accountRepository;

    @GetMapping
    List<TransactionDto> getallTransactions(Principal principal) {
        return createTransactionService.getAllTransactionsForUser(principal.getName());
    }

    @PostMapping("/account_id")
    List<TransactionDto> getTranactionsForAccount(@RequestBody AccountDto accountDto, Principal principal) {
        return createTransactionService.getAllTransactionsForAccount(accountDto.getIdAccount(), principal.getName());
    }

    @PostMapping("/create_transaction")
    void createTransaction(@RequestBody TransactionDto transactionDto, Principal principal) {
        List<Long> listAccountId = new ArrayList<>();
        for (AccountModel accountModel : accountRepository.findAllByUserModelEmail(principal.getName())) {
            listAccountId.add(accountModel.getId());
        }
        createTransactionService.writeTransaction(transactionDto);
    }

}
