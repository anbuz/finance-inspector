package ru.buz.financeinspector.Controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.buz.financeinspector.dao.repository.UserRepository;
import ru.buz.financeinspector.service.AuthorizationService;
import ru.buz.financeinspector.service.UserDto;

@RequiredArgsConstructor
@RestController
@RequestMapping("/registration")
public class RegistrationController {
    private final AuthorizationService authorizationService;


    @PostMapping
    String createNewUser(@RequestBody UserDto userDto) {

        if (authorizationService.createUser(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getEmail(),
                userDto.getPassword())
        )
            return "success";
        else return "try again";
    }
}
