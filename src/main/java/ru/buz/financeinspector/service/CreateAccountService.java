package ru.buz.financeinspector.service;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import ru.buz.financeinspector.converter.ConverterDtoToDao;
import org.springframework.stereotype.Service;
import ru.buz.financeinspector.dao.Models.UserModel;
import ru.buz.financeinspector.dao.repository.AccountRepository;
import ru.buz.financeinspector.dao.repository.AccountTypeRepository;
import ru.buz.financeinspector.dao.repository.UserRepository;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class CreateAccountService {
    private final AccountRepository accountRepository;
    private final ConverterDtoToDao converterDtoToDao;
    private final UserRepository userRepository;
    private final AccountTypeRepository accountTypeRepository;


    @Transactional
    public Boolean createNewAccount(AccountDto accountDto) {
        long backId =
                accountRepository
                        .save(converterDtoToDao
                                .convert(accountDto,
                                        accountTypeRepository.getOne(accountDto.getAccountType()),
                                        userRepository.getOne(accountDto.getIdUser()))).getId();
        return backId > 0;
    }


    public boolean verificationNumOfAccount(long idUser) {
        return accountRepository.findAccountModelsByUserModelId(idUser).size() < 5;
    }

}

