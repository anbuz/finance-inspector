package ru.buz.financeinspector.service;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountTypeDto {
    private Integer transactionId;
    private String transactionName;
}
