package ru.buz.financeinspector.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Component
public class DigestServiceSha implements DigestService {
    private final PasswordEncoder passwordEncoder;

    public String hash(String pass) {
        String password;
        password = passwordEncoder.encode(pass);
        return password;
    }
}
