package ru.buz.financeinspector.service;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TattDto {
    private long idTransaction;
    private int idTransactionType;
}
