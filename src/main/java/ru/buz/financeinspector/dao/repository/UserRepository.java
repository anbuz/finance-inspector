package ru.buz.financeinspector.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.buz.financeinspector.dao.Models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
    UserModel findByEmailAndPassword(String name, String pass);

    UserModel findByEmail(String name);


}
