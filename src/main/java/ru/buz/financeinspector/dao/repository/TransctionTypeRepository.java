package ru.buz.financeinspector.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.buz.financeinspector.dao.Models.TransactionsTypeModel;

public interface TransctionTypeRepository extends JpaRepository<TransactionsTypeModel, Long> {
}
