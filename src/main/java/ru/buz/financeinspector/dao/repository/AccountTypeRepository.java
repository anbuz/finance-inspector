package ru.buz.financeinspector.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.buz.financeinspector.dao.Models.AccountTypeModel;

public interface AccountTypeRepository extends JpaRepository<AccountTypeModel, Long> {
}
