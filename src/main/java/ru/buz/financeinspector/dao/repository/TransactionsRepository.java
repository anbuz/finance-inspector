package ru.buz.financeinspector.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.buz.financeinspector.dao.Models.TransactionsModel;

import java.util.List;

public interface TransactionsRepository extends JpaRepository<TransactionsModel, Long> {
    List<TransactionsModel> findAllByToIdAccountId(Long id);
}
