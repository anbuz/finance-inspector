package ru.buz.financeinspector.dao.Models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TattModel {
    private long idTransaction;
    private int idTransactionType;
}
